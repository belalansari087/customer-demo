package com.customer.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.customer.dto.Customer;
import com.customer.dto.Response;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {
	
	@PostMapping("/")
	public ResponseEntity<Response<String>> addCustomer(@RequestBody Customer cust) {
		
		System.out.println(cust);
		
		Response<String> resp = new Response<>();
		resp.setStatus(200);
		resp.setResult("Success");
		return new ResponseEntity<>(resp, HttpStatus.CREATED);
	}
	
	@PostMapping("/")
	public ResponseEntity<Response<List<Customer>>> listCustomer() {
		List<Customer> list = new ArrayList<Customer>();
		
		Response<List<Customer>> respList = new Response<>();
		respList.setStatus(200);
		respList.setResult(list);
		
		return new ResponseEntity<>(respList, HttpStatus.OK);
	}
}
